import _root_.sbt.Keys._

name := "FilmwebAPI"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies += "net.ruippeixotog" %% "scala-scraper" % "0.1.2"
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.0-M15"
libraryDependencies += "org.scalamock" % "scalamock-scalatest-support_2.11" % "3.2.2"
libraryDependencies += "org.jsoup" % "jsoup" % "1.8.3"


    