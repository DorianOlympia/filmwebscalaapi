package com.danskidawid.filmwebAPI.utilities

import org.jsoup.nodes.{Document, Element}

import scala.collection.JavaConversions._

/**
 * Created by Dawid on 2016-02-20.
 */
trait MovieUtilsComponent {
  val movieUtils: MovieUtils = new MovieUtils

  class MovieUtils extends ProductionUtils{
    def director(htmlDoc: Document): Seq[String] = {
      val dirRow = htmlDoc.getElementsByAttributeValue("class", "filmInfo bottom-15").get(0).getElementsByTag("tr").get(0)
      val directors = dirRow.getElementsByTag("a")
      for(director: Element <- directors) yield director.text

    }

    def scriptwriter(htmlDoc: Document): Seq[String] = {
      val dirRow = htmlDoc.getElementsByAttributeValue("class", "filmInfo bottom-15").get(0).getElementsByTag("tr").get(1)
      val directors = dirRow.getElementsByTag("a")
      for(director: Element <- directors) yield director.text
    }

    def genres(htmlDoc: Document): Seq[String] = {
      try {
        val genresLinks = htmlDoc.getElementsByAttributeValue("class", "inline sep-comma genresList").get(0).getElementsByTag("a")
        for (element <- genresLinks) yield element.text
      }
      catch {
        case e: Exception => Vector.empty
      }
    }
  }
}
