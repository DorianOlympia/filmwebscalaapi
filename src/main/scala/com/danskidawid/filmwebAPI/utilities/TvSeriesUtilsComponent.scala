package com.danskidawid.filmwebAPI.utilities

import org.jsoup.nodes.Document
import collection.JavaConversions._

trait TvSeriesUtilsComponent {
  val tvSeriesUtils: TvSeriesUtils = new TvSeriesUtils

  class TvSeriesUtils extends ProductionUtils
  {
    def numberOfSeasons(htmlDoc: Document): String = {
      val numberOfElements = htmlDoc.getElementsByAttributeValue("class", "list inline sep-line")
        .get(0).children().size()
      numberOfElements.toString
    }

    def createdBy(htmlDoc: Document): Seq[String] = {
      val authors = htmlDoc.getElementsByAttributeValue("class", "filmInfo bottom-15").get(0).getElementsByTag("tr").get(0).getElementsByTag("a")
      for(author <- authors)
        yield author.text()
    }

    def genres(htmlDoc: Document): Seq[String] = {
      try {
        val genresLinks = htmlDoc.getElementsByAttributeValue("class", "filmInfo bottom-15").get(0).getElementsByTag("tr").get(1).getElementsByTag("a")
        for (element <- genresLinks) yield element.text
      }
      catch {
        case e: Exception => Vector.empty
      }
    }
  }
}