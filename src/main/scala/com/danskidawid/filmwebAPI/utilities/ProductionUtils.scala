package com.danskidawid.filmwebAPI.utilities

import com.danskidawid.filmwebAPI.entities.Production.Role
import org.jsoup.nodes.{Document, Element}
import collection.JavaConversions._

trait ProductionUtils {
  def title(htmlDoc: Document) = {
    try
      htmlDoc.getElementsByAttributeValue("class", "inline filmTitle").get(0).text()
    catch {
      case e: Exception => ""
    }
  }

  def description(htmlDoc: Document) = {
    try
      htmlDoc.getElementsByAttributeValue("class", "filmPlot bottom-15").get(0).text()
    catch {
      case e: Exception => ""
    }
  }

  def rating(htmlDoc: Document) = {
    try
      htmlDoc.getElementsByAttributeValue("property", "v:average").get(0).text().replace(',', '.')
    catch {
      case e: Exception => "N/A"
    }
  }

  def actors(htmlDoc: Document): Seq[Role] = {
    try {
      val actorsRows = htmlDoc.getElementsByAttributeValue("class", "filmCast filmCastCast").get(0).getElementsByTag("tr")
      actorsRows.remove(0)
      for (actor <- actorsRows) yield {
        val name = actor.getElementsByTag("td").get(1).text()
        val role = {
          val tmp = actor.getElementsByTag("td").get(3).text()
          if(tmp == "") "N/A" else tmp
        }
        new Role(name, role)
      }
    }
    catch {
      case e: Exception => Vector.empty
    }
  }

  def votes(htmlDoc: Document) = {
    try {
      htmlDoc.getElementsByAttributeValue("property", "v:votes").get(0).text().replaceAll(" ", "")
    }
    catch {
      case e: Exception => "N/A"
    }
  }
}
