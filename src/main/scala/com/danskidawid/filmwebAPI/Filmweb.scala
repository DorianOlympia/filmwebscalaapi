package com.danskidawid.filmwebAPI

import com.danskidawid.filmwebAPI.entities.{TvSeries, Movie}
import com.danskidawid.filmwebAPI.utilities.{TvSeriesUtilsComponent, MovieUtilsComponent}
import net.ruippeixotog.scalascraper.browser.Browser
import org.jsoup.Jsoup
import org.jsoup.nodes.{Element, Document}

/**
 * Created by Dawid on 2016-02-18.
 */
object Filmweb{
  def getFilmwebService = new FilmwebService(new Browser) with MovieUtilsComponent with TvSeriesUtilsComponent
}

class FilmwebService(val browser: Browser){
  this : MovieUtilsComponent with TvSeriesUtilsComponent =>
  val filmwebURL = "http://www.filmweb.pl"

  def movieByHTML(htmlDoc: Document, url: String) ={
    val tit = movieUtils.title(htmlDoc)
    val des = movieUtils.description(htmlDoc)
    val rat = movieUtils.rating(htmlDoc)
    val gen = movieUtils.genres(htmlDoc)
    val act = movieUtils.actors(htmlDoc)
    val dir = movieUtils.director(htmlDoc)
    val scr = movieUtils.scriptwriter(htmlDoc)
    val vot = movieUtils.votes(htmlDoc)
    Movie(url, tit, des, rat, gen, act, vot, scr, dir)
  }

  def movieByURL(url: String) ={
    movieByHTML(browser.get(url), url)
  }

  def seriesByHTML(htmlDoc: Document, url: String) ={
    val tit = tvSeriesUtils.title(htmlDoc)
    val des = tvSeriesUtils.description(htmlDoc)
    val rat = tvSeriesUtils.rating(htmlDoc)
    val gen = tvSeriesUtils.genres(htmlDoc)
    val act = tvSeriesUtils.actors(htmlDoc)
    val aut = tvSeriesUtils.createdBy(htmlDoc)
    val nos = tvSeriesUtils.numberOfSeasons(htmlDoc)
    val vot = tvSeriesUtils.votes(htmlDoc)
    TvSeries(url, tit, des, rat, gen, act, vot, aut, nos)
  }

  def seriesByURL(url: String) = {
    seriesByHTML(browser.get(url), url)
  }
}
