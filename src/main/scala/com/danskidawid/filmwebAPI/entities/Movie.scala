package com.danskidawid.filmwebAPI.entities

import Production.Role


case class Movie(val url: String, val title: String, val description: String, val rating: String, val genre: Seq[String],
                 val mainActors: Seq[Role], val votes: String, val scriptwriter: Seq[String], val directors: Seq[String])
  extends Production {

  override def toString = {
    super.toString + '\n' +
      "Directors: " + directors.mkString(", ") + '\n' +
      "Scriptwriters: " + scriptwriter.mkString(", ")
  }
}




