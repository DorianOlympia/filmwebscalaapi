package com.danskidawid.filmwebAPI.entities

import com.danskidawid.filmwebAPI.entities.Production.Role

/**
 * Created by Dawid on 2016-02-28.
 */
case class TvSeries(val url:String, val title: String, val description: String, val rating: String, val genre: Seq[String],
                val mainActors: Seq[Role], val votes: String, val authors: Seq[String], val numberOfSeasons: String)
  extends Production{

  override def toString = {
    super.toString + '\n' +
      "Authors: " + authors.mkString(", ") + '\n' +
      "Seasons: " + numberOfSeasons.mkString(", ")
  }
}


