package com.danskidawid.filmwebAPI.entities

import com.danskidawid.filmwebAPI.entities.Production.Role

trait Production {
  val url: String
  val title: String
  val description: String
  val rating: String
  val genre: Seq[String]
  val mainActors: Seq[Role]
  val votes: String

  override def toString: String = {
    {
      "URL: " + url + '\n' +
        "Title: " + title + '\n' +
        "Description: " + description + '\n' +
        "Rating: " + rating + s"($votes)" + '\n' +
        "Genre: " + genre.mkString(", ") + '\n' +
        "Main actors: " + mainActors.mkString(", ")
    }
  }
}

object Production {

  class Role(val actor: String, val role: String) {
    override def toString = actor + " jako " + role
  }

}

