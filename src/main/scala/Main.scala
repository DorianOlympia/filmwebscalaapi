import java.io.{PrintWriter, File}

import com.danskidawid.filmwebAPI.Filmweb
import net.ruippeixotog.scalascraper.browser.Browser

object Main extends App{
  val m = Filmweb.getFilmwebService
  print(m.seriesByURL("http://www.filmweb.pl/serial/Gra+o+tron-2011-476848"))
  print("\n\n\n")
  print(m.movieByURL("http://www.filmweb.pl/Wiezien.Nienawisci"))
}
