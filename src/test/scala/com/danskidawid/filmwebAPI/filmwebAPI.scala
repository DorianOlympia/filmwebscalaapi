package com.danskidawid

import java.io.File

import com.danskidawid.filmwebAPI.entities.{TvSeries, Production, Movie}
import Production.Role
import net.ruippeixotog.scalascraper.browser.Browser
import org.jsoup.Jsoup

import scala.io.Source


package object filmwebAPI {
  val invalidURL = "http://www.filmweb.pl/fam/D213dsp2l-34aswq675"

  //example movie
  val exampleMovieTitle = "Wyspa skazańców"
  val exampleMovieDesc = "Zbuntowany siedemnastolatek trafia do odizolowanego na wyspie poprawczaka. Ośrodek jest rządzony przez sadystycznych opiekunów."
  val exampleMovieVotes = "34354"
  val exampleMovieURL = "http://www.filmweb.pl/film/Wyspa+skazancow-2010-478478"
  val exampleMovieRating = "7.6"
  val exampleMovieGenres = Vector("Dramat")
  val exampleMovieActors = Vector(new Role("Benjamin Helstad", "Erling / C-19"), new Role("Trond Nilssen", "Olav / C-1"),
    new Role("Stellan Skarsgård", "Dyrektor szkoły"), new Role("Kristoffer Joner", "Bråthen"),
    new Role("Magnus Langlete", "Ivar / C-5"), new Role("Morten Løvstad", "Øystein"), new Role("Daniel Berg", "Johan"),
    new Role("Odin Gineson Brøderud", "Axel"))
  val exampleMovieDirectors = Vector("Marius Holst")
  val exampleMovieScriptWriters = Vector("Dennis Magnusson", "Eric Schmid")

  //example series
  val exampleSeriesDesc = "Dzięki swojej fotograficznej pamięci i ponadprzeciętnej inteligencji Mike Ross zdobywa pracę u Harveya Spectera. Jednak firma, w której pracuje, zatrudnia jedynie prawników z Uniwersytetu Harvarda. "
  val exampleSeriesURL = "http://www.filmweb.pl/serial/W+garniturach-2011-585726"
  val exampleSeriesRating = "8,3"
  val exampleSeriesGenres = Vector("Dramat", "Prawniczy")
  val exampleSeriesActors = Vector(new Role("Gabriel Macht", "Harvey Reginald Specter"),
    new Role("Patrick J. Adams", "Michael \"Mike\" James Ross"), new Role("Rick Hoffman", "Louis Litt"),
    new Role("Meghan Markle", "Rachel Zane"), new Role("Sarah Rafferty", "Donna Paulsen"),
    new Role("Gina Torres", "Jessica Lourdes Pearson"), new Role("Tom Lipinski", "Trevor Evans"),
    new Role("Vanessa Ray", "Jennifer Jenny Griffith"))
  val exampleNumOfSeasons = "6"
  val exampleSeriesCreatedBy = Vector("Aaron Korsh")


  val browser = new Browser
  val validMovieHtml = browser.parseFile(new File(getClass.getClassLoader.getResource("testPage.html").getPath))
  val validTvSeriesHtml = browser.parseFile(new File(getClass.getClassLoader.getResource("seriesTestPage.html").getPath))
  val invalidHtmlDoc = browser.parseFile(new File(getClass.getClassLoader.getResource("wrontTestPage.html").getPath))
  val exampleSeriesVotes = "74497"
  val exampleMovie = Movie(exampleMovieURL, exampleMovieTitle, exampleMovieDesc, exampleMovieRating, exampleMovieGenres, exampleMovieActors,
    exampleMovieVotes, exampleMovieScriptWriters, exampleMovieDirectors)
  val exampleSeriesTitle = "W garniturach"
  val exampleTvSeries = TvSeries(exampleSeriesURL, exampleSeriesTitle, exampleSeriesDesc, exampleSeriesRating, exampleSeriesGenres, exampleSeriesActors,
    exampleSeriesVotes, exampleSeriesCreatedBy, exampleNumOfSeasons)
}

