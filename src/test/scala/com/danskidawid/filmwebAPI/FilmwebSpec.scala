package com.danskidawid.filmwebAPI

import com.danskidawid.filmwebAPI.utilities.{TvSeriesUtilsComponent, MovieUtilsComponent}
import net.ruippeixotog.scalascraper.browser.Browser
import org.jsoup.nodes.Document
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec


class FilmwebSpec extends FlatSpec with MockFactory {

  trait FakeMovieUtilsComponent extends MovieUtilsComponent {
    override val movieUtils = stub[MovieUtils]
    (movieUtils.description _).when(validMovieHtml).returns(exampleMovieDesc)
    (movieUtils.director _).when(validMovieHtml).returns(exampleMovieDirectors)
    (movieUtils.scriptwriter _).when(validMovieHtml).returns(exampleMovieScriptWriters)
    (movieUtils.actors _).when(validMovieHtml).returns(exampleMovieActors)
    (movieUtils.genres _).when(validMovieHtml).returns(exampleMovieGenres)
    (movieUtils.votes _).when(validMovieHtml).returns(exampleMovieVotes)
    (movieUtils.rating _).when(validMovieHtml).returns(exampleMovieRating)
    (movieUtils.title _).when(validMovieHtml).returns(exampleMovieTitle)
  }

  trait FakeTvSeriesUtilsComponent extends TvSeriesUtilsComponent {
    override val tvSeriesUtils = stub[TvSeriesUtils]
    (tvSeriesUtils.description _).when(validTvSeriesHtml).returns(exampleSeriesDesc)
    (tvSeriesUtils.createdBy _).when(validTvSeriesHtml).returns(exampleSeriesCreatedBy)
    (tvSeriesUtils.numberOfSeasons _).when(validTvSeriesHtml).returns(exampleNumOfSeasons)
    (tvSeriesUtils.genres _).when(validTvSeriesHtml).returns(exampleSeriesGenres)
    (tvSeriesUtils.votes _).when(validTvSeriesHtml).returns(exampleSeriesVotes)
    (tvSeriesUtils.rating _).when(validTvSeriesHtml).returns(exampleSeriesRating)
    (tvSeriesUtils.title _).when(validTvSeriesHtml).returns(exampleSeriesTitle)
    (tvSeriesUtils.actors _).when(validTvSeriesHtml).returns(exampleSeriesActors)
  }

  class FakeMovieBrowser extends Browser{
    override def get(url: String): Document = {
      validMovieHtml
    }
  }

  class FakeSeriesBrowser extends Browser{
    override def get(url: String): Document = {
      validTvSeriesHtml
    }
  }

  "Filmweb.movieByHTML" should "return valid Movie for valid html given" in {
    val filmwebService = new FilmwebService(new FakeMovieBrowser) with FakeMovieUtilsComponent with TvSeriesUtilsComponent
    assert(filmwebService.movieByHTML(validMovieHtml, exampleMovieURL) == exampleMovie)
  }

  "Filmweb.movieByURL" should "return valid Production object for valid URL provided" in {
    val filmwebService = new FilmwebService(new FakeMovieBrowser) with FakeMovieUtilsComponent with TvSeriesUtilsComponent
    assert(filmwebService.movieByURL(exampleMovieURL) == exampleMovie)
  }

  it should "throw an exception for invalid URL provided" in {
    val filmwebService = new FilmwebService(new Browser) with FakeMovieUtilsComponent with TvSeriesUtilsComponent
    intercept[Exception]{
      filmwebService.movieByURL(invalidURL)
    }
  }

  "Filmweb.SeriesByHTML" should "return valid TvSeries for valid html given" in {
    val filmwebService = new FilmwebService(new FakeMovieBrowser) with MovieUtilsComponent with FakeTvSeriesUtilsComponent
    assert(filmwebService.seriesByHTML(validTvSeriesHtml, exampleSeriesURL) == exampleTvSeries)
  }

  "Filmweb.seriesByURL" should "return valid Production object for valid URL provided" in {
    val filmwebService = new FilmwebService(new FakeSeriesBrowser) with MovieUtilsComponent with FakeTvSeriesUtilsComponent
    assert(filmwebService.seriesByURL(exampleSeriesURL) == exampleTvSeries)
  }

  it should "throw an exception for invalid URL provided" in {
    val filmwebService = new FilmwebService(new Browser) with MovieUtilsComponent with FakeTvSeriesUtilsComponent
    intercept[Exception]{
      filmwebService.movieByURL(invalidURL)
    }
  }
}
