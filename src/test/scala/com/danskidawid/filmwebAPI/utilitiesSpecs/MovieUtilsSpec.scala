package com.danskidawid.filmwebAPI.utilitiesSpecs

import com.danskidawid.filmwebAPI._
import com.danskidawid.filmwebAPI.utilities.MovieUtilsComponent
import org.scalatest.FlatSpec

/**
 * Created by Dawid on 2016-02-28.
 */
class MovieUtilsSpec extends FlatSpec{
  val movieUtilsComp = new MovieUtilsComponent {}

  "movieUtils.director()" should "return list of directors for proper html file provided" in {
    val directors = movieUtilsComp.movieUtils.director(validMovieHtml)
    assert(exampleMovieDirectors == directors)
  }

  "movieUtils.scriptwriter()" should "return list of scriptwriters for proper html file provided" in {
    val scriptwriters = movieUtilsComp.movieUtils.scriptwriter(validMovieHtml)
    assert(exampleMovieScriptWriters == scriptwriters)
  }

  "generes()" should "return valid generes for valid html input" in {
    val generes = movieUtilsComp.movieUtils.genres(validMovieHtml)
    assert(generes == exampleMovieGenres)
  }
}
