package com.danskidawid.filmwebAPI.utilitiesSpecs

import com.danskidawid.filmwebAPI._
import com.danskidawid.filmwebAPI.utilities.ProductionUtils
import org.scalatest.FlatSpec


class ProductionUtilsSpec extends FlatSpec{
  val prodUtils = new { val name = "Testing"} with ProductionUtils

  "title()" should "return valid title for valid html input" in {
    val title = prodUtils.title(validMovieHtml)
    assert(title == exampleMovieTitle)
  }

  it should "return valid title for valid html series input" in {
    val title = prodUtils.title(validTvSeriesHtml)
    assert(title == exampleSeriesTitle)
  }

  "description()" should "return valid description for valid html input" in {
    val description = prodUtils.description(validMovieHtml)
    assert(description == exampleMovieDesc)
  }

  "rating()" should "return valid rating for valid html input" in {
    val rating = prodUtils.rating(validMovieHtml)
    assert(rating == exampleMovieRating)
  }

  "actors()" should "return valid actors for valid html input" in {
    val actors = prodUtils.actors(validMovieHtml)
    for(actor <- actors)
      assert(exampleMovieActors.map(_.toString).contains(actor.toString))
  }

  "votes()" should "return valid votes for valid html input" in {
    val votes = prodUtils.votes(validMovieHtml)
    assert(votes == exampleMovieVotes)
  }
}
