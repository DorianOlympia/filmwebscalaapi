package com.danskidawid.filmwebAPI.utilitiesSpecs

import com.danskidawid.filmwebAPI.utilities.TvSeriesUtilsComponent
import org.scalatest.FlatSpec
import com.danskidawid.filmwebAPI._
/**
 * Created by Dawid on 2016-02-22.
 */
class TvSeriesUtilsSpec extends FlatSpec {
  val tvSeriesUtilsComp = new TvSeriesUtilsComponent {}

  ".numberOfSeasons" should "return proper number of Seasons for proper htmlDoc" in {
    val seasons = tvSeriesUtilsComp.tvSeriesUtils.numberOfSeasons(validTvSeriesHtml)
    assert(seasons == exampleNumOfSeasons)
  }


  ".createdBy" should "return proper createdBy info for proper htmlDoc" in {
    val createdBy = tvSeriesUtilsComp.tvSeriesUtils.createdBy(validTvSeriesHtml)
    assert(createdBy == exampleSeriesCreatedBy)
  }

  "generes()" should "return valid generes for valid html input" in {
    val generes = tvSeriesUtilsComp.tvSeriesUtils.genres(validTvSeriesHtml)
    assert(generes == exampleSeriesGenres)
  }
}
