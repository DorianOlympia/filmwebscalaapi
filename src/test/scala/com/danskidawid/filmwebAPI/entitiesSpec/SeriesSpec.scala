package com.danskidawid.filmwebAPI.entitiesSpec

import com.danskidawid.filmwebAPI._
import org.scalatest.FlatSpec

/**
 * Created by Dawid on 2016-02-28.
 */
class SeriesSpec extends FlatSpec{
  "TvSeries()" should "create valid object for valid arguments" in {
    val series = exampleTvSeries
    assert(exampleSeriesTitle == series.title)
    assert(exampleSeriesDesc == series.description)
    assert(exampleSeriesURL == series.url)
    assert(exampleSeriesRating == series.rating)
    assert(exampleSeriesGenres == series.genre)
    assert(exampleSeriesActors == series.mainActors)
    assert(exampleSeriesCreatedBy == series.authors)
    assert(exampleNumOfSeasons== series.numberOfSeasons)
  }

  "toString" should "return valid info about the Production" in {
    val series = exampleTvSeries.toString
    assert(series.contains(exampleSeriesTitle))
    assert(series.contains(exampleSeriesDesc))
    assert(series.contains(exampleSeriesURL))
    assert(series.contains(exampleSeriesRating.toString))
    assert(series.contains(exampleSeriesVotes.toString))
    for (genre <- exampleSeriesGenres) assert(series.contains(genre))
    for (actor <- exampleSeriesActors) assert(series.contains(actor.toString))
    for (swriter <- exampleSeriesCreatedBy) assert(series.contains(swriter))

  }
}
