package com.danskidawid.filmwebAPI.entitiesSpec

import com.danskidawid.filmwebAPI._
import org.scalatest.FlatSpec

class MovieSpec extends FlatSpec {

  "Movie()" should "create valid object for valid arguments" in {
    val movie = exampleMovie
    assert(exampleMovieTitle == movie.title)
    assert(exampleMovieDesc == movie.description)
    assert(exampleMovieURL == movie.url)
    assert(exampleMovieRating == movie.rating)
    assert(exampleMovieGenres == movie.genre)
    assert(exampleMovieActors == movie.mainActors)
    assert(exampleMovieScriptWriters == movie.scriptwriter)
    assert(exampleMovieDirectors == movie.directors)
  }

  "toString" should "return valid info about the Production" in {
    val movie = exampleMovie.toString
    assert(movie.contains(exampleMovieTitle))
    assert(movie.contains(exampleMovieDesc))
    assert(movie.contains(exampleMovieURL))
    assert(movie.contains(exampleMovieRating.toString))
    assert(movie.contains(exampleMovieVotes.toString))
    for (genre <- exampleMovieGenres) assert(movie.contains(genre))
    for (actor <- exampleMovieActors) assert(movie.contains(actor.toString))
    for (swriter <- exampleMovieScriptWriters) assert(movie.contains(swriter))
    for (dir <- exampleMovieDirectors) assert(movie.contains(dir))
  }
}
